"""Module to generate plot panels for figure in the main manuscript and supplementary files."""

import pathlib
import typing

import matplotlib.axes
import matplotlib.cm
import matplotlib.colors
import matplotlib.lines
import matplotlib.pyplot
import pandas
import scipy.stats
import seaborn
import upsetplot

import py_utils.cox
import py_utils.format_pvalue
import py_utils.kaplan_meier
import py_utils.survival_stats

PALETTE_TSN = ["#e31a1c", "#fb9a99", "#1f78b4"]
PALETTE_TN = ["#e31a1c", "#1f78b4"]
PALETTE_T = ["#e31a1c"]

TISSUE_ORDER = ["tumour", "invasive front", "normal"]


def _cic_events_by_grouping_var_plot_helper(
    data: pandas.DataFrame,
    grouping_var: str,
    palette: typing.Union[str, typing.List[str]] = "Greens",
    ax: matplotlib.axes.Axes = None,
) -> None:
    """
    Helper function to plot cell-in-cell events by a grouping_var.
    :param data: dataframe with clinical and cell-in-cell events data to plot, as returned by
                 `data_util.clinical_and_cic_events()`;
    :param grouping_var: variable to group data for plotting;
    :param palette: palette to use for plotting;
    :param ax: axes to plot into or current axes (gca) if None.
    """
    data2 = data.copy()

    data2.dropna(subset=[grouping_var], inplace=True)
    data2 = data2.reset_index()
    data2.tissue = pandas.Categorical(data2.tissue, TISSUE_ORDER)
    #  Rename categories in categorical column to include the n-numbers for core and patients:
    data2["tissue"].cat.rename_categories(
        {
            key: f"{key}\nc={sub_data.shape[0]}\np={sub_data.universal_patient_id.nunique()}"
            for (key, sub_data) in data2.groupby("tissue")
        },
        inplace=True,
    )
    data2[grouping_var].cat.rename_categories(
        {
            key: f"{key} (c={sub_data.shape[0]}, p={sub_data.universal_patient_id.nunique()})"
            for (key, sub_data) in data2.groupby(grouping_var)
        },
        inplace=True,
    )

    if ax is None:
        ax = matplotlib.pyplot.gca()

    lineplot_args = {
        "x": "tissue",
        "y": "events",
        "hue": grouping_var,
        "style": grouping_var,
        "dashes": False,
        "ax": ax,
        "palette": palette,
        "data": data2.reset_index(),
    }

    seaborn.lineplot(markers=False, alpha=0.1, legend=False, **lineplot_args)
    seaborn.lineplot(
        markers=True,
        err_style="bars",
        ci=95,
        alpha=1,
        lw=2,
        markersize=4,
        legend="full",
        **lineplot_args,
    )

    ax.set_xlabel("Tissue")
    ax.set_ylabel("CIC events [median #, 95%CI]")
    ax.legend(
        markerscale=0.5,
        fontsize="x-small",
        loc="upper center",
        bbox_to_anchor=(0.5, 1.4),
        fancybox=True,
        shadow=True,
    )


def _kaplan_meier_plot_helper(data: pandas.DataFrame, cic_var: str, name: str) -> None:
    """
    Helper function to plot figure for Kaplan-Meier estimates for disease free survival (DFS) and disease specific
    survival grouped by a cell-in-cell events variable.
    :param data: dataframe with clinical and cell-in-cell events, as returned by
                `data_util.export_data_for_supplementary_figure_6()`;
    :param cic_var: name of column in 'data' with cell-in-cell data to group by for plots;
    :param name: label to use to name the figure.
    """

    plot_data = data.copy().dropna(
        subset=["DFS_months", "DFS_event", "DSS_months", "DSS_event", cic_var]
    )
    plot_data["by_group"] = plot_data[cic_var]

    fig, axes = matplotlib.pyplot.subplots(
        nrows=1,
        ncols=2,
        figsize=(6, 4),
        gridspec_kw={
            "left": 0.1,
            "bottom": 0.325,
            "right": 0.98,
            "wspace": 0.35,
            "hspace": 0.5,
        },
    )
    axes = axes.flatten()

    params = dict(
        include_n_in_legend=True,
        fontsize=6,
        xticks_interval=24,
        colors=["#000000", "#000000", "#1f78b4", "#1f78b4"],
        censor_marker_size=5,
        linewidth=1,
        linestyles=["dotted", "solid", "dotted", "solid"],
        has_legend=False,
        include_per_term_stats=True,
    )

    py_utils.kaplan_meier.plot(
        plot_data.DFS_months,
        plot_data.DFS_event,
        plot_data["by_group"],
        ylabel="DFS",
        ax=axes[0],
        **params,
    )
    py_utils.kaplan_meier.plot(
        plot_data.DSS_months,
        plot_data.DSS_event,
        plot_data["by_group"],
        ylabel="DSS",
        ax=axes[1],
        **params,
    )

    handles, labels = axes[0].get_legend_handles_labels()
    fig.legend(
        handles,
        labels,
        ncol=2,
        loc="center",
        bbox_to_anchor=(0.5, 0.1),
        bbox_transform=fig.transFigure,
        fancybox=True,
        shadow=True,
    )

    fig.savefig(pathlib.Path("figures_and_tables", f"{name}.pdf"))


def figure_6_panel_b(data: pandas.DataFrame) -> None:
    """
    Generate figure 6 panel b.
    Plot figure panel with heterogeneity of cell-in-cell events detected in multiple cores of individual patients.
    :param data: dataframe, one row per patient and multi-level columns including ('median') for
                           individual tissues, as returned by `data_util.aggregate_cic()`.
    """
    # Select only tumour cores and sort by median number of cell-in-cell events
    data = (
        data.copy()
        .stack("tissue")
        .query('tissue=="tumour"')
        .sort_values(by="median", ascending=False)
    )

    fig = matplotlib.pyplot.figure(figsize=(7, 5))
    ax = seaborn.pointplot(
        x="universal_patient_id",
        y="median",
        scale=0.5,
        sort=False,
        color="k",
        data=data.reset_index(),
    )
    ax.fill_between(
        range(data.shape[0]), data["min"], data["max"], alpha=0.1, color="k"
    )
    ax.set_xlim(-1, data.shape[0])
    ax.set_xticklabels([])
    ax.set_xlabel("Patients (n={})".format(data.shape[0]))
    ax.set_ylabel("Cell-in-cell events in tumour tissue [median, (min, max)]")
    ax.set_title("Tumour")

    matplotlib.pyplot.tight_layout()
    fig.savefig(pathlib.Path("figures_and_tables", "figure_6_panel_b.pdf"))


def figure_6_panel_c(data: pandas.DataFrame) -> None:
    """
    Generate figure 6 panel c.
    Plot figure panel with cell-in-cell events (median and 95% confidence intervals) as function of tissue types
    ('tumour', 'invasive front', 'normal').
    :param data: tall dataframe, one row per cell-in-cell events detected per patient per tissue per slide per marker,
                 as returned by `data_util.clinical_and_cic_events()`.
    """
    fig = matplotlib.pyplot.figure(figsize=(3.5, 5))
    data = data.loc[["NI240"]].reset_index("cohort")
    data.cohort.cat.remove_unused_categories(inplace=True)
    _cic_events_by_grouping_var_plot_helper(data, "cohort", palette=["k"])
    matplotlib.pyplot.tight_layout()
    fig.savefig(pathlib.Path("figures_and_tables", "figure_6_panel_c.pdf"))


def supplementary_figure_1(
    cli: pandas.DataFrame, cic: pandas.DataFrame, proteins: pandas.DataFrame
) -> None:
    """
    Generate supplementary figure 1.
    Plot figure panel with upset-style figure to describe data availability of clinical characteristics and measurements
    of cell-in-cell events and proteins.
    :param cli: dataframe with clinical data, one row per patient, as returned by `data_util.clinical()`;
    :param cic: tall dataframe with cell-in-cell events data, as returned by `data_util.cic()`;
    :param proteins: tall dataframe with protein straining scores, as returned by `data_util.proteins()`.
    """
    cli = cli.copy()
    cli.loc[:, "DFS and DSS survival information"] = (
        cli[["DFS_months", "DFS_event", "DSS_months", "DSS_event"]].notna().all(axis=1)
    )

    has_cic = cic.groupby(["universal_patient_id", "tissue"]).size().unstack().notna()
    has_cic.columns = [f"CIC measurements in {x} tissue" for x in has_cic.columns]
    data = cli[["DFS and DSS survival information"]].join(has_cic, how="inner")

    data.loc[:, "Measurements for all proteins in tumour and normal tissue"] = (
        proteins.groupby(["universal_patient_id", "tissue", "protein"])
        .size()
        .unstack(["tissue", "protein"])
        .notna()
        .all(axis=1)
    )

    with matplotlib.rc_context(rc={"font.size": 6}):
        fig = matplotlib.pyplot.figure()
        upsetplot.plot(
            data.groupby(data.columns.to_list()).size(),
            show_counts=True,
            sort_by="cardinality",
            intersection_plot_elements=10,
            totals_plot_elements=5,
            element_size=20,
            fig=fig,
        )
        fig.subplots_adjust(left=0.05, bottom=0.05, right=0.98, top=0.98)
        fig.set_size_inches(7, 5)
        fig.savefig(pathlib.Path("figures_and_tables", "supplementary_figure_1.pdf"))


def supplementary_figure_2(data: pandas.DataFrame) -> None:
    """
    Generate supplementary figure 2.
    Plot figure panel with scatterplot comparing cell-in-cell events detected from tissue microarray sections stained
    with haematoxylin/eosin (HE) and Hematoxylin/Tyrosine-Protein Kinase Met (cMET).
    :param data: tall dataframe with cell-in-cell events data, as returned by `data_util.cic()`.
    """
    data_w = (
        data.groupby(["universal_patient_id", "tissue", "marker"])
        .median()
        .unstack("marker")
        .dropna()
    )
    data_w.columns = data_w.columns.droplevel()

    data_w = data_w.join(
        data_w.groupby(["HE", "cMET"]).size().to_frame("count"), on=["HE", "cMET"]
    )

    params = dict(x="HE", y="cMET", data=data_w.reset_index())
    seaborn.jointplot(
        marginal_kws=dict(bins=10, kde=True, color="k", rug=True),
        kind="reg",
        scatter=False,
        space=1,
        color="k",
        **params,
    )
    seaborn.scatterplot(
        hue="tissue",
        hue_order=TISSUE_ORDER,
        palette=PALETTE_TSN,
        markers="tissue",
        size="count",
        sizes=(50, 200),
        alpha=0.2,
        edgecolor="k",
        **params,
    )

    ax = matplotlib.pyplot.gca()

    tau, pvalue = scipy.stats.kendalltau(data_w.HE, data_w.cMET)
    ax.set_title(
        (
            "Kendall rank correlation tau={:.2f}, {}\n"
            "from n={:.0f} aggregated quantifications\n"
            "from n={:.0f} distinct patients"
        ).format(
            tau,
            py_utils.format_pvalue.format_pvalue(pvalue, pvalue_prefix="P"),
            data_w.shape[0],
            len(data_w.index.get_level_values("universal_patient_id").unique()),
        )
    )
    ax.set_xlabel(
        "Cell-in-cell events from HE-stained cores\n [median # per patient per tissue]"
    )
    ax.set_ylabel(
        "Cell-in-cell events from cMET-stained cores\n[median # per patient per tissue]"
    )

    matplotlib.pyplot.tight_layout()
    ax.figure.savefig(pathlib.Path("figures_and_tables", "supplementary_figure_2.pdf"))


def supplementary_figure_3(data: pandas.DataFrame) -> None:
    """
    Generate supplementary figure 3.
    Plot figure panel with cell-in-cell events distribution grouped by tissue type ('tumour', 'invasive front',
    'normal').
    :param data: tall dataframe with cell-in-cell events data, as returned by `data_util.cic()`.
    """
    g = seaborn.FacetGrid(
        row="tissue",
        row_order=TISSUE_ORDER,
        hue="tissue",
        hue_order=TISSUE_ORDER,
        palette=PALETTE_TSN,
        sharex=True,
        sharey=True,
        aspect=4,
        height=1,
        data=data.reset_index(),
    )

    g.map(
        seaborn.kdeplot, "events", clip_on=False, shade=True, alpha=0.8, bw=0.4, cut=0
    )
    g.map(seaborn.kdeplot, "events", clip_on=False, linewidth=2, bw=0.4, cut=0)
    g.map(matplotlib.pyplot.axhline, y=0, linewidth=3, clip_on=False)

    ax = matplotlib.pyplot.gca()
    ax.set_xlim(0, data.events.max())

    g.set_titles("{row_name}")
    g.set_xlabels("Cell-in-cell events [#]")
    g.set_ylabels("Frequency")

    matplotlib.pyplot.tight_layout()
    g.savefig(pathlib.Path("figures_and_tables", "supplementary_figure_3.pdf"))


def supplementary_figure_4(data: pandas.DataFrame) -> None:
    """
    Generate supplementary figure 4.
    Plot figure panel with cell-in-cell events (median and 95% CI) detected in tissue microarray sections prepared from
    tumour centre, invasive front and matched normal tissue broken down by clinico-pathological characteristics. The
    letters "c" and "p" are abbreviations for "cores" and "patients", respectively. See `supplementary_table_7()` for
    corresponding statistical analysis.
    :param data: dataframe with clinical and cell-in-cell events, as returned by `data_util.clinical_and_cic_events()`.
    """
    fig, axes = matplotlib.pyplot.subplots(nrows=4, ncols=3, figsize=(7, 10))
    axes = axes.flatten()

    _cic_events_by_grouping_var_plot_helper(data.loc["NI240"], "stage", "Reds", axes[0])
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"], "T_stage", "Reds", axes[1]
    )
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"], "N_stage", "Reds", axes[2]
    )
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"], "treatment", ["#000000", "#1f78b4"], axes[3]
    )
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"],
        "chemotherapy_cycles_bin",
        ["#000000", "#a6cee3", "#1f78b4"],
        axes[4],
    )
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"], "radiotherapy", "Set1", axes[5]
    )
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"], "age_bin", ["#6baed6", "#2171b5", "#1f78b4"], axes[6]
    )
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"], "sex", ["#6baed6", "#fa9fb5"], axes[7]
    )
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"], "type_of_surgery", ["#4daf4a", "#984ea3", "#ff7f00"], axes[8]
    )
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"], "lymphovascular_invasion", ["#377eb8", "#e41a1c"], axes[9]
    )
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"], "cancer_type", ["#377eb8", "#ff7f00"], axes[10]
    )
    _cic_events_by_grouping_var_plot_helper(
        data.loc["NI240"],
        "tumour_site",
        ["#a6cee3", "#1f78b4", "#ff7f00", "#4daf4a"],
        axes[11],
    )

    matplotlib.pyplot.subplots_adjust(
        left=0.07, bottom=0.07, right=0.97, top=0.95, wspace=0.45, hspace=0.95
    )
    fig.savefig(pathlib.Path("figures_and_tables", "supplementary_figure_4.pdf"))


def supplementary_figure_5(data: pandas.DataFrame) -> None:
    """
    Generate supplementary figure 5.
    Plot figure panel with association between expression of key proteins involved in TRAIL signalling and cell-in-cell
    events. Protein expression was determined by immunohistochemistry in tissue microarray sections prepared from tumour
    and normal tissue and expressed as the product of the staining scores for intensity (0-3 integer scale) and coverage
    (0-4 integer scale). Protein expression across multiple biological replicas (cores) per patient per tissue type was
    aggregated by median and plotted grouped by absence (CIC=0) or presence (CIC>0) of CIC events in the corresponding
    tissue. Protein expression by tissue type and color-coded by CIC events group is shown as violin plot overlaid with
    individual measurements shown as swarmplot. Distribution quartiles are highlighted by tick dotted black lines.
    See `supplementary_table_8()` for corresponding statistical analysis (restricted to tumour tissue).
    :param data: dataframe with aggregated data for cell-in-cell events and proteins, as returned by
                 `make_tables._supplementary_table_8_helper()`.
    """
    params = dict(x="tissue", y="protein_median", hue="cic_events_class", dodge=True)

    g = seaborn.catplot(
        col="protein",
        col_wrap=3,
        kind="violin",
        cut=0,
        inner="quartile",
        palette=["#a6cee3", "#fb9a99"],
        legend=False,
        sharex=False,
        sharey=True,
        saturation=1,
        data=data.reset_index(),
        **params,
    )

    # Make a copy of the private legend data without the swarmplot.
    legend_data = g._legend_data.copy()

    g.map_dataframe(seaborn.swarmplot, size=3, palette=["#1f78b4", "#e31a1c"], **params)

    for line in g.fig.findobj(matplotlib.lines.Line2D):
        # Modify quartile line objects to be more visible:
        if line.axes is None:
            continue
        line.set_color("k")
        line.set_linewidth(3)
        line.set_zorder(3)

    g.set_titles("{col_name}")
    g.set_xlabels("Tissue")
    g.set_ylabels("Protein expression [median]")

    g.fig.legend(
        legend_data.values(),
        legend_data.keys(),
        ncol=2,
        loc="right",
        bbox_to_anchor=(0.9, 0.25),
        fancybox=True,
        shadow=True,
        title="Cell-in-cell events by tissue",
        bbox_transform=g.fig.transFigure,
    )

    matplotlib.pyplot.savefig(
        pathlib.Path("figures_and_tables", "supplementary_figure_5.pdf"),
        bbox_inches="tight",
    )


def figure_6_panels_f_g_h_and_supplementary_figures_7_8_9(
    data: pandas.DataFrame
) -> None:
    """
    Generate figure 6 panel f, g and h and supplementary figures 7, 8 and 9.
    Plot figure panel with Kaplan-Meier estimates of DFS and DSS comparing patients of the NI240 cohort grouped by the
    absence or presence of cell-in-cell events detected in tissue:
        - figure 6 panel f: stage II and III patients grouped by CIC events detected in 'invasive front' tissue;
        - figure 6 panel g: stage II patients grouped by CIC events detected in 'invasive front' tissue;
        - figure 6 panel h: stage III patients grouped by CIC events detected in 'invasive front' tissue;
        - supplementary figure 7: stage II and III patients grouped by CIC events detected in 'tumour' tissue;
        - supplementary figure 8: stage II patients grouped by CIC events detected in 'tumour' tissue;
        - supplementary figure 9: stage III patients grouped by CIC events detected in 'tumour' tissue;
    :param data: dataframe with clinical and cell-in-cell events, as returned by
                `data_util.export_data_for_supplementary_figure_6()`.
    """
    _kaplan_meier_plot_helper(
        data, "cic_events_class_invasive_front", "figure_6_panel_f"
    )
    _kaplan_meier_plot_helper(
        data.query('stage=="2"'), "cic_events_class_invasive_front", "figure_6_panel_g"
    )
    _kaplan_meier_plot_helper(
        data.query('stage=="3"'), "cic_events_class_invasive_front", "figure_6_panel_h"
    )

    _kaplan_meier_plot_helper(data, "cic_events_class_tumour", "supplementary_figure_7")
    _kaplan_meier_plot_helper(
        data.query('stage=="2"'), "cic_events_class_tumour", "supplementary_figure_8"
    )
    _kaplan_meier_plot_helper(
        data.query('stage=="3"'), "cic_events_class_tumour", "supplementary_figure_9"
    )
