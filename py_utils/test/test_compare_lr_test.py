import unittest

import lifelines
import numpy
import pandas
import rpy2.robjects
import rpy2.robjects.packages
import rpy2.robjects.pandas2ri
import statsmodels.formula.api

import py_utils.compare_lr_test


class CompareLRTestCase(unittest.TestCase):
    @staticmethod
    def test_matched_logit_anova_r():
        """
        Test that compare_lr_test returns matching results to car::Anova type III test for a logistic regression model.
        """
        data = pandas.read_csv("university_admission.csv", dtype={"rank": "category"})

        # Calculate anova type III test with R as golden:
        stats = rpy2.robjects.packages.importr("stats")
        car = rpy2.robjects.packages.importr("car")

        with rpy2.robjects.conversion.localconverter(
            rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter
        ):
            mod_gpa_gre_rank = stats.glm(
                "admit ~ gpa + gre + rank",
                data=data,
                family=stats.binomial(link="logit"),
            )
            golden = car.Anova(mod_gpa_gre_rank, type="III")

        full_model = statsmodels.formula.api.logit(
            "admit ~ gpa + gre + rank", data
        ).fit(disp=False)
        no_gpa = statsmodels.formula.api.logit("admit ~ gre + rank", data).fit(
            disp=False
        )
        no_gre = statsmodels.formula.api.logit("admit ~ gpa + rank", data).fit(
            disp=False
        )
        no_rank = statsmodels.formula.api.logit("admit ~ gpa + gre", data).fit(
            disp=False
        )

        numpy.testing.assert_allclose(
            py_utils.compare_lr_test.compare_lr_test(full_model, no_gpa),
            golden.loc["gpa", ["LR Chisq", "Pr(>Chisq)", "Df"]],
        )

        numpy.testing.assert_allclose(
            py_utils.compare_lr_test.compare_lr_test(full_model, no_gre),
            golden.loc["gre", ["LR Chisq", "Pr(>Chisq)", "Df"]],
        )

        numpy.testing.assert_allclose(
            py_utils.compare_lr_test.compare_lr_test(full_model, no_rank),
            golden.loc["rank", ["LR Chisq", "Pr(>Chisq)", "Df"]],
        )

    @staticmethod
    def test_matched_cox_anova_r():
        """
        Test that compare_lr_test returns matching results to car::Anova type III test for a Cox regression model.
        """

        data = pandas.read_csv(
            "lung.csv",
            dtype={"ph.karno": "category", "sex": "category"},
            usecols=["time", "status", "sex", "age", "ph.karno"],
        )
        data.dropna(inplace=True)
        data["status"] = data.status == 2

        # Calculate anova type III test with R as golden:
        survival = rpy2.robjects.packages.importr("survival")
        car = rpy2.robjects.packages.importr("car")

        with rpy2.robjects.conversion.localconverter(
            rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter
        ):
            cox_mod = survival.coxph(
                rpy2.robjects.Formula("Surv(time, status) ~ sex + age + ph.karno"),
                data=data,
            )
            golden = car.Anova(cox_mod, type="III")

        cox = lifelines.CoxPHFitter().fit(
            pandas.get_dummies(data, drop_first=True),
            duration_col="time",
            event_col="status",
        )
        no_sex = lifelines.CoxPHFitter().fit(
            pandas.get_dummies(data.drop(columns=["sex"]), drop_first=True),
            duration_col="time",
            event_col="status",
        )
        no_age = lifelines.CoxPHFitter().fit(
            pandas.get_dummies(data.drop(columns=["age"]), drop_first=True),
            duration_col="time",
            event_col="status",
        )
        no_ph_karno = lifelines.CoxPHFitter().fit(
            pandas.get_dummies(data.drop(columns=["ph.karno"]), drop_first=True),
            duration_col="time",
            event_col="status",
        )

        numpy.testing.assert_allclose(
            py_utils.compare_lr_test.compare_lr_test(cox, no_sex),
            golden.loc["sex", ["LR Chisq", "Pr(>Chisq)", "Df"]],
        )

        numpy.testing.assert_allclose(
            py_utils.compare_lr_test.compare_lr_test(cox, no_age),
            golden.loc["age", ["LR Chisq", "Pr(>Chisq)", "Df"]],
        )

        numpy.testing.assert_allclose(
            py_utils.compare_lr_test.compare_lr_test(cox, no_ph_karno),
            golden.loc["ph.karno", ["LR Chisq", "Pr(>Chisq)", "Df"]],
        )


if __name__ == "__main__":
    unittest.main()
