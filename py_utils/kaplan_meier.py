import typing

import lifelines
import lifelines.statistics
import matplotlib.axes
import matplotlib.colors
import matplotlib.pyplot
import numpy
import pandas
import seaborn

import py_utils.format_pvalue


def plot(
    times: pandas.Series,
    has_events: pandas.Series,
    groups: pandas.Series,
    xlim: typing.Tuple[float, float] = (0, 120),
    xticks_interval: float = 24,
    xlabel: str = "Time [months]",
    ylabel: str = "Overall survival",
    title: str = "",
    colors: typing.List = seaborn.color_palette("Set1", 12),
    censor_marker_size: int = 8,
    linewidth: float = 1,
    linestyles: typing.List[str] = ["solid", "dotted", "dashed", "dashdot"] * 3,
    has_legend: bool = True,
    bbox_to_anchor: typing.Tuple[float, float, float, float] = (0, 1.01, 1, 0.1),
    legend_kwargs: typing.Dict = {},
    include_only_logrank_p_stats: bool = False,
    include_per_term_stats: bool = True,
    include_n_in_legend: bool = True,
    stats_loc: typing.Tuple[float, float] = (0.01, 0.01),
    fontsize: str = "small",
    ax: matplotlib.axes.Axes = None,
) -> None:
    """
    Plot Kaplan-Meier estimates.
    :param times: series with time until event/censoring for each subject/sample/observation;
    :param has_events: series with boolean with same length as times indicating observations with events (if True);
    :param groups: series with same length as times including group information;
    :param xlim: tuple, lower and upper range of the x axis;
    :param xticks_interval: interval to space xticks;
    :param xlabel: label for x axis;
    :param ylabel: label for y axis;
    :param title: title of the plot;
    :param colors: colors to use to plot Kaplan-Meier estimates for individual groups;
    :param censor_marker_size: size of the marker to indicate censored observations;
    :param linewidth: width of the Kaplan-Meier line;
    :param linestyles: line style for each group (see
           https://matplotlib.org/stable/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_linestyle).
           If the list has fewer elements than there are groups, the line styles repeat;
    :param has_legend: whether to include a legend;
    :param bbox_to_anchor: bbox_to_anchor coordinates to place legend box;
    :param legend_kwargs: additional keyword arguments for legend;
    :param include_only_logrank_p_stats: whether to print only logrank p-value stats in the plot;
    :param include_per_term_stats: whether to print per-term stats in the plot;
    :param include_n_in_legend: whether to include number of subjects/samples/observations per group in legend;
    :param stats_loc: coordinates, in normalized units, where to place stats text;
    :param fontsize: string with fontsize specification, one of xx-small, x-small, small, medium, large, x-large,
           xx-large, larger, smaller;
    :param ax: axes to plot into or current axes (gca) if None.
    """
    colors = [matplotlib.colors.hex2color(hex_color) for hex_color in colors]

    kmf = lifelines.KaplanMeierFitter()
    if ax is None:
        ax = matplotlib.pyplot.gca()
    for idx, group in enumerate(groups.astype("category").cat.categories):
        if include_n_in_legend:
            label = "{} (n={})".format(group, (groups == group).sum())
        else:
            label = group
        kmf.fit(times.loc[groups == group], has_events[groups == group], label=label)
        kmf.plot(
            ax=ax,
            show_censors=True,
            censor_styles={"ms": censor_marker_size},
            ci_show=False,
            linewidth=linewidth,
            linestyle=linestyles[idx],
            color=colors[idx],
        )
    ax.set_xlim(left=xlim[0], right=xlim[1])
    ax.set_xticks(numpy.arange(xlim[0], xlim[1] + 1, xticks_interval))
    ax.set_ylim(0.0, 1.05)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)

    if has_legend:
        ax.legend(**legend_kwargs)
        ax.legend(
            bbox_to_anchor=bbox_to_anchor,
            loc="lower left",
            mode="expand",
            borderaxespad=0.0,
            fontsize=fontsize,
            fancybox=True,
            **legend_kwargs
        )
    else:
        legend = ax.legend()
        legend.remove()

    # Annotate with stats
    stats = lifelines.statistics.multivariate_logrank_test(times, groups, has_events)
    cph = lifelines.CoxPHFitter()
    cph.fit(
        pandas.concat(
            [
                pandas.DataFrame({"times": times, "has_events": has_events}),
                pandas.get_dummies(groups, prefix=groups.name, drop_first=True),
            ],
            axis=1,
        ),
        duration_col="times",
        event_col="has_events",
    )
    overall_stats = "${:s}$, ${:s}$, $c_{{i}}={:.2f}$".format(
        py_utils.format_pvalue.format_pvalue(stats.p_value, pvalue_prefix="p_{LR}"),
        py_utils.format_pvalue.format_pvalue(
            cph.log_likelihood_ratio_test().p_value, pvalue_prefix="p_{LRT}"
        ),
        cph.concordance_index_,
    )
    per_term_stats = [
        "{}: HR {:.2f} ({:.2f}-{:.2f})".format(
            level.replace(
                groups.name + "_", groups.astype("category").cat.categories[0] + " vs. "
            ),
            row["exp(coef)"],
            row["exp(coef) lower 95%"],
            row["exp(coef) upper 95%"],
        )
        for (level, row) in cph.summary.iterrows()
    ]

    if include_only_logrank_p_stats:
        stats = "${:s}$".format(
            py_utils.format_pvalue.format_pvalue(stats.p_value, pvalue_prefix="p_{LR}")
        )
    else:
        if include_per_term_stats:
            stats = "\n".join([overall_stats, *per_term_stats])
        else:
            stats = overall_stats

    ax.text(
        stats_loc[0],
        stats_loc[1],
        stats,
        transform=ax.transAxes,
        verticalalignment="bottom",
        fontsize=fontsize,
    )
