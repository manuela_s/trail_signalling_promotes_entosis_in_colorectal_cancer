import pathlib
import tempfile

import matplotlib.axes
import matplotlib.cm
import matplotlib.colors
import matplotlib.lines
import matplotlib.pyplot
import numpy
import pandas
import pydot
import svgutils
import svgutils.compose

import make_tables


def _plot_trail_signaling_by_cic_status(
    protein_concs: pandas.Series,
    significant_difference: pandas.Series,
    cm: matplotlib.colors.Colormap,
    norm: matplotlib.colors.Normalize,
    filename: pathlib.Path,
) -> None:
    """
    Helper function for `figure_6_panel_d()`.
    Generate graphviz plot with concentrations for different proteins.
    :param protein_concs: series with the protein concentrations. Index should have protein name;
    :param significant_difference: series to indicate for each protein if there is a significant difference;
    :param filename: filename to generate.
    """

    def add_node(container: pydot.Graph, protein_name: str) -> None:
        """
        Add node representing one protein to a pydot Graph.
        :param container: the graph to add the node to;
        :param protein_name: the name of the protein to add.
        """
        if significant_difference[protein_name] == "P<0.05":
            node_args = dict(style='"filled,bold"', penwidth=5)
        elif significant_difference[protein_name] == "0.05<=P<0.1":
            node_args = dict(style='"filled,bold"', penwidth=2)
        else:
            node_args = dict(style='"filled,dotted"')
        container.add_node(
            pydot.Node(
                name=protein_name,
                fillcolor=matplotlib.colors.to_hex(
                    cm(norm(protein_concs[protein_name]))
                ),
                **node_args,
            )
        )

    dot = pydot.Dot(compound=True)

    add_node(dot, "TRAIL")

    dr = pydot.Cluster("DR", rank="same")
    add_node(dr, "DR4")
    add_node(dr, "DR5")
    dot.add_subgraph(dr)

    death = pydot.Cluster("death")
    add_node(death, "CASP8")
    add_node(death, "FLIP")
    dot.add_subgraph(death)

    dot.add_edge(pydot.Edge("TRAIL", "DR4", lhead=dr.get_name()))
    dot.add_edge(
        pydot.Edge("DR4", "CASP8", ltail=dr.get_name(), lhead=death.get_name())
    )

    dot.write_svg(filename)


def _plot_color_legend(
    cm: matplotlib.colors.Colormap,
    norm: matplotlib.colors.Normalize,
    output_dir: pathlib.Path,
    size: float = 0.5,
) -> None:
    """
    Create color legend for relative protein expression by cell-in-cell event status.
    :param cm: colormap to use for the legend;
    :param norm: normalization used from protein expression to color;
    :param output_dir: directory to write svg image file to;
    :param size: height of the figure, in inches.
    """
    pal = [cm(norm(conc)) for conc in [0.5, 0.75, 1, 1.5, 2]]
    xticklabels = ["1/2 X", "3/4 X", "1X", "3/2 X", "2X"]

    n = len(pal)
    f, ax = matplotlib.pyplot.subplots(1, 1, figsize=(n * size, size))
    ax.imshow(
        numpy.arange(n).reshape(1, n),
        cmap=matplotlib.colors.ListedColormap(list(pal)),
        interpolation="nearest",
        aspect="auto",
    )
    ax.set_xticks(numpy.arange(n))
    ax.set_xticklabels(xticklabels)
    ax.set_yticks([-0.5, 0.5])
    ax.set_yticklabels([])
    ax.set_title("Relative protein expression")

    matplotlib.pyplot.savefig(output_dir / "pathway_color_legend.svg")


def _plot_significance_legend(output_dir: pathlib.Path) -> None:
    """
    Create node edge style legend to indicate statistical significance for protein expression by cell-in-cell event
    status.
    :param output_dir: directory to write svg image file to.
    """
    dot_legend = pydot.Dot()
    dot_legend.add_node(
        pydot.Node(
            name="significant",
            label="P<0.05",
            fillcolor="white",
            style='"filled,bold"',
            penwidth=5,
        )
    )
    dot_legend.add_node(
        pydot.Node(
            name="almost_significant",
            label="0.05<=P<0.1",
            fillcolor="white",
            style='"filled,bold"',
            penwidth=2,
        )
    )
    dot_legend.add_node(
        pydot.Node(
            name="not_significant",
            label="P>=0.1",
            fillcolor="white",
            style='"filled,dotted"',
        )
    )
    dot_legend.write_svg(output_dir / "pathway_significance_legend.svg")


def _compose_svg_figure(component_dir: pathlib.Path) -> None:
    """
    Compose the different components of the figure into a single svg.
    :param component_dir: directory to read the different component files from.
    """
    svgutils.compose.Figure(
        300 + 232,
        320,
        svgutils.compose.Panel(
            svgutils.compose.SVG(
                component_dir / "proteins_by_cic_class_pathway_no_cic.svg"
            ),
            svgutils.compose.Text("CIC=0", 25, 20, size=12, weight="bold"),
        ),
        svgutils.compose.Panel(
            svgutils.compose.SVG(
                component_dir / "proteins_by_cic_class_pathway_cic.svg"
            ),
            svgutils.compose.Text("CIC>0", 25, 20, size=12, weight="bold"),
        ).move(300, 0),
        svgutils.compose.Panel(
            svgutils.compose.SVG(component_dir / "pathway_color_legend.svg")
        ).move((300 + 232 - 180) / 2, 210),
        svgutils.compose.Panel(
            svgutils.compose.SVG(component_dir / "pathway_significance_legend.svg")
        ).move((300 + 232 - 309) / 2, 270),
    ).save(pathlib.Path("figures_and_tables", "figure_6_panel_d.svg"))


def figure_6_panel_d(
    cic_aggregated: pandas.DataFrame, proteins_aggregated: pandas.DataFrame
) -> None:
    """
    Generate figure 6 panel d.
    Plot figure panel with
    :param cic_aggregated: dataframe with aggregated cell-in-cell events data, as returned by
                           `data_util.aggregate_cic()`;
    :param proteins_aggregated: dataframe with aggregated protein staining, as returned by
                                `data_util.aggregate_proteins()`.
    """
    data = (
        pandas.Series(
            (cic_aggregated.loc["NI240"]["median"].tumour.dropna() == 0).map(
                {True: "CIC=0", False: "CIC>0"}
            ),
            name="cic_tumour_class",
        )
        .to_frame()
        .join(
            proteins_aggregated.loc["NI240"][["protein_median"]].query(
                'tissue=="tumour"'
            ),
            how="inner",
        )
    )

    data = (
        data.groupby(["cic_tumour_class", "protein"])
        .protein_median.mean()
        .unstack("cic_tumour_class")
    )

    stats = make_tables.supplementary_table_8(
        make_tables._supplementary_table_8_helper(
            cic_aggregated, proteins_aggregated
        ).query('tissue=="tumour"')
    )
    pvalues = (
        stats["Unadjusted statistics"]
        .str.extract("(P[=|<][0|1].[0-9]+)", expand=False)
        .str.replace("P[=|<]", "")
        .astype("float64")
    )
    pvalues = pvalues.to_frame("pvalue").reset_index("tissue", drop=True)
    pvalues.loc[:, "significant_difference"] = pandas.cut(
        pvalues.pvalue,
        bins=[0, 0.05, 0.1, 1],
        labels=["P<0.05", "0.05<=P<0.1", "P>=0.1"],
        right=False,
    )

    rel_data = (data / data.mean(axis=1).values.reshape([-1, 1])).fillna(1)

    cm = matplotlib.cm.RdBu_r
    norm = matplotlib.colors.LogNorm(vmin=0.5, vmax=2)
    tmp_dir = tempfile.TemporaryDirectory()
    output_dir = pathlib.Path(tmp_dir.name)

    _plot_trail_signaling_by_cic_status(
        rel_data["CIC=0"],
        pvalues.significant_difference,
        cm,
        norm,
        output_dir / "proteins_by_cic_class_pathway_no_cic.svg",
    )
    _plot_trail_signaling_by_cic_status(
        rel_data["CIC>0"],
        pvalues.significant_difference,
        cm,
        norm,
        output_dir / "proteins_by_cic_class_pathway_cic.svg",
    )

    _plot_color_legend(cm, norm, output_dir)
    _plot_significance_legend(output_dir)

    _compose_svg_figure(output_dir)
