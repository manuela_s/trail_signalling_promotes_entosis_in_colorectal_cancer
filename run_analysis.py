"""Module to run analysis included in the main manuscript and supplementary files."""

import pathlib

import matplotlib

import data_util
import figure_6_panel_d
import make_figures
import make_tables

PALETTE_TSN = ["#e31a1c", "#fb9a99", "#1f78b4"]
PALETTE_TN = ["#e31a1c", "#1f78b4"]
PALETTE_T = ["#e31a1c"]


if __name__ == "__main__":
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["font.size"] = 8
    pathlib.Path("figures_and_tables").mkdir(exist_ok=True)

    # Clinical data
    cli = data_util.clinical()

    # Cell-in-cell data
    cic = data_util.cic()
    cic_aggregated = data_util.aggregate_cic(cic)

    # Proteins data
    proteins = data_util.proteins()

    proteins_aggregated = data_util.aggregate_proteins(proteins)

    j = data_util.export_data_for_supplementary_figure_6(
        cli.loc["NI240"], cic_aggregated.loc["NI240"], proteins_aggregated.loc["NI240"]
    )

    cli_cic = data_util.clinical_and_cic_events(cli, cic)

    make_figures.figure_6_panel_b(cic_aggregated.loc["NI240"])
    make_figures.figure_6_panel_c(cli_cic)
    figure_6_panel_d.figure_6_panel_d(cic_aggregated, proteins_aggregated)
    make_figures.figure_6_panels_f_g_h_and_supplementary_figures_7_8_9(j)

    make_figures.supplementary_figure_1(
        cli.loc["NI240"], cic.loc["NI240"], proteins.loc["NI240"]
    )
    make_figures.supplementary_figure_2(cic.loc["NI240"])
    make_figures.supplementary_figure_3(cic.loc["NI240"])
    make_figures.supplementary_figure_4(cli_cic)
    make_figures.supplementary_figure_5(
        make_tables._supplementary_table_8_helper(cic_aggregated, proteins_aggregated)
    )

    make_tables.supplementary_tables_9_and_10(j)
