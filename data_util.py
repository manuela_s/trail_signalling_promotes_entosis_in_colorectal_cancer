"""Module to load clinical, cell-in-cell events and protein expression for patients of the NI240 cohort."""

import pathlib
import pandas

TISSUE_ORDER = ["tumour", "invasive front", "normal"]


def clinical() -> pandas.DataFrame:
    """
    Get clinical data.
    :return: dataframe with:
             - indices: 'cohort' and 'universal_patient_id';
             - columns: 'clinical_site', 'stage', 'detailed_stage', 'T_stage', 'N_stage', 'M_stage', 'grade', 'age',
                        'sex', 'tumour_site', 'detailed_tumour_site', 'type_of_surgery', 'lymphovascular_invasion',
                        'radiotherapy', 'received_randomised_treatment', 'treatment', 'chemotherapy_full_dose_given',
                        'chemotherapy_dose_reduction_reason', 'chemotherapy_number_cycles_delayed',
                        'chemotherapy_within_6weeks_of_surgery', 'chemotherapy_at_2weekly_intervals', 'DFS_months',
                        'DFS_event', 'DSS_months', 'DSS_event', 'age_bin', 'chemotherapy_cycles_bin', 'cancer_type' and
                        'stage_treatment'.
    """
    data = pandas.read_csv(
        pathlib.Path("data", "clinical.csv"),
        index_col=["cohort", "universal_patient_id"],
        dtype={
            "cohort": "category",
            "clinical_site": "category",
            "study_id": "category",
            "stage": "category",
            "detailed_stage": "category",
            "T_stage": "category",
            "N_stage": "category",
            "M_stage": "category",
            "treatment": "category",
            "sex": "category",
            "DFS_months": "Int16",
            "OS_months": "Int16",
            "DFS_event": "Int16",
            "OS_event": "Int16",
            "received_randomised_treatment": "category",
            "grade": "category",
            "age": "float",
            "tumour_site": "category",
            "detailed_tumour_site": "category",
            "type_of_surgery": "category",
            "lymphovascular_invasion": "category",
            "radiotherapy": "category",
            "chemotherapy_full_dose_given": "category",
            "chemotherapy_dose_reduction_reason": "category",
            "chemotherapy_number_cycles_delayed": "Int16",
            "chemotherapy_within_6weeks_of_surgery": "category",
            "chemotherapy_at_2weekly_intervals": "category",
            "DSS_months": "Int16",
            "DSS_event": "Int16",
            "age_bin": "category",
            "chemotherapy_cycles_bin": "category",
        },
    )

    data.sex.cat.reorder_categories(["male", "female"], inplace=True)
    data.age_bin.cat.reorder_categories(
        ["under 60", "60 to 70", "over 70"], inplace=True
    )

    data.received_randomised_treatment.cat.reorder_categories(
        ["randomized", "not_randomized"], inplace=True
    )
    data.treatment.cat.reorder_categories(["observation", "chemotherapy"], inplace=True)
    data.chemotherapy_cycles_bin.cat.reorder_categories(
        ["none", "incomplete", "full"], inplace=True
    )
    data["cancer_type"] = pandas.Categorical(
        data.tumour_site.map(
            {"proximal": "colon", "distal": "colon", "rectal": "rectum"}
        ),
        ["colon", "rectum"],
    )
    data.tumour_site.cat.reorder_categories(
        ["proximal", "distal", "rectal", "synchronous"], inplace=True
    )
    data.detailed_tumour_site.cat.reorder_categories(
        [
            "ascending_colon",
            "descending_colon",
            "transverse_colon",
            "caecum",
            "sigmoid_colon",
            "rectosigmoid",
            "rectum",
            "synchronous",
        ],
        inplace=True,
    )

    data.type_of_surgery.cat.reorder_categories(
        ["resection", "colectomy", "hemicolectomy"], inplace=True
    )
    data.lymphovascular_invasion.cat.reorder_categories(
        ["no-invasion", "invasion"], inplace=True
    )

    data["stage_treatment"] = pandas.Categorical(
        "stage" + data["stage"].astype("str") + "_" + data["treatment"].astype("str")
    )

    # Group together sub-groups with low number of patients
    data.loc[:, "T_stage"] = pandas.Categorical(
        data.T_stage.replace({"1": "1-2", "2": "1-2"}), ["1-2", "3", "4"]
    )
    data.loc[:, "N_stage"] = pandas.Categorical(
        data.N_stage.replace({"2": "2-3", "3": "2-3"}), ["0", "1", "2-3"]
    )

    # Drop patients that were not randomized to treatment
    data = data.query('received_randomised_treatment=="randomized"')

    # Drop patients with a clinically rare (low n) and distinct phenotype (stage)
    data = data[(~data.stage.isin(["1", "4"]))]
    data.stage.cat.remove_unused_categories(inplace=True)

    return data


def cic() -> pandas.DataFrame:
    """
    Get cell-in-cell events data.
    :return: dataframe with:
             - indices: 'cohort', 'universal_patient_id', 'tissue', 'marker', 'slide' and 'sub_cohort';
             - column: 'events'.
    """
    data = pandas.read_csv(
        pathlib.Path("data", "cell_in_cell_events.csv"),
        index_col=[
            "cohort",
            "universal_patient_id",
            "sub_cohort",
            "tissue",
            "slide",
            "marker",
        ],
        dtype={"cohort": "category", "tissue": "category"},
    )

    # Keep cell-in-cell events dat if there are at least 2 replicas per patient per tissue
    replicas_per_patient_per_tissue = (
        data.groupby(["cohort", "universal_patient_id", "tissue"]).size().to_frame("n")
    )
    replicas_per_patient_per_tissue = replicas_per_patient_per_tissue[
        replicas_per_patient_per_tissue.n >= 2
    ]
    data = data.join(replicas_per_patient_per_tissue[[]], how="inner")

    return data


def aggregate_cic(data: pandas.DataFrame) -> pandas.DataFrame:
    """
    Aggregate cell-in-cell events across measurements performed on multiple tissue microarray cores.
    :param data: dataframe as returned by `data()`;
    :return: dataframe, one row per patient with:
             - indices 'cohort' and 'universal_patient_id';
             - columns: 2 level column headers:
                        - statistical aggregation ('count', 'median', 'low_ci', 'high_ci', 'min', 'max', 'iqr') and
                          tissue ('normal', 'invasive front', 'tumour').
    """
    by_patient_tissue = data.groupby(
        ["cohort", "universal_patient_id", "tissue"]
    ).events.describe()[["25%", "50%", "75%", "min", "max", "count"]]
    by_patient_tissue.rename(
        columns={"25%": "low_ci", "50%": "median", "75%": "high_ci"}, inplace=True
    )
    by_patient_tissue["iqr"] = (
        by_patient_tissue["high_ci"] - by_patient_tissue["low_ci"]
    )

    by_patient_tissue = by_patient_tissue.unstack("tissue")

    return by_patient_tissue


def proteins():
    """
    Get protein staining score data.
    :return: dataframe with:
             - indices: 'cohort', 'universal_patient_id', 'protein', 'tissue' and 'replica';
             - column: 'score'.
    """
    return pandas.read_csv(
        pathlib.Path("data", "ihc_proteins.csv"),
        index_col=["cohort", "universal_patient_id", "protein", "tissue", "replica"],
    )


def aggregate_proteins(data: pandas.DataFrame) -> pandas.DataFrame:
    """
    Aggregate protein scores across immunohistochemistry measurements performed on multiple tissue microarray cores.
    :param data: dataframe as returned by `proteins()`;
    :return: dataframe, one row per patient with:
             - indices 'cohort', 'universal_patient_id', 'protein' and 'tissue';
             - columns: 'protein_count', 'protein_median' and 'protein_iqr'.
    """
    protein_stats = data.groupby(
        ["cohort", "universal_patient_id", "protein", "tissue"]
    ).score.describe()
    protein_stats.rename(
        columns={"25%": "low_ci", "75%": "high_ci", "50%": "median"}, inplace=True
    )
    protein_stats.columns = ["protein_" + col for col in protein_stats.columns]

    protein_stats["protein_iqr"] = (
        protein_stats["protein_high_ci"] - protein_stats["protein_low_ci"]
    )
    protein_stats["protein_range"] = (
        protein_stats["protein_max"] - protein_stats["protein_min"]
    )
    protein_stats["protein_cv"] = (
        protein_stats["protein_std"] / protein_stats["protein_mean"]
    )
    protein_stats["protein_iqr_over_median"] = (
        protein_stats["protein_iqr"] / protein_stats["protein_median"]
    )

    return protein_stats[["protein_count", "protein_median", "protein_iqr"]].dropna()


def clinical_and_cic_events(
    clinical_data: pandas.DataFrame, cic_data: pandas.DataFrame
) -> pandas.DataFrame:
    """
    Get clinical data and cell-in-cell events data in a single dataframe.
    :param clinical_data: dataframe with clinical data, as returned by `clinical()`;
    :param cic_data: dataframe with cell-in-cell events data, as returned by `cic()`;
    :return: tall dataframe, one row per cell-in-cell events detected per patient per tissue per slide per marker.
    """
    clinical_and_cic_data = clinical_data.join(cic_data, how="inner")
    clinical_and_cic_data.reset_index("cohort", inplace=True)
    clinical_and_cic_data["cohort"] = clinical_and_cic_data.cohort.astype("category")
    clinical_and_cic_data.set_index("cohort", append=True, inplace=True)
    clinical_and_cic_data = clinical_and_cic_data.reorder_levels(
        ["cohort", "universal_patient_id", "sub_cohort", "marker", "slide", "tissue"]
    )

    return clinical_and_cic_data


def export_data_for_supplementary_figure_6(
    cli: pandas.DataFrame,
    cic_aggregated: pandas.DataFrame,
    proteins_aggregated: pandas.DataFrame,
) -> pandas.DataFrame:
    """
    Prepare and export data required by matlab HCP toolbox (Supplementary Figure 6 in Supplementary File 1).
    :param cli: dataframe as returned by `clinical_drop_rare_phenotype()`;
    :param cic_aggregated: dataframe as returned by `aggregate_cic`;
    :param proteins_aggregated: dataframe as returned by `aggregate_proteins()`;
    :param name: label for filename;
    :return: dataframe, one row per patient with:
             - indices 'cohort' and 'universal_patient_id';
             - columns: 'clinical_site', 'stage', 'detailed_stage', 'T_stage', 'N_stage', 'M_stage', 'grade', 'age',
                        'sex', 'tumour_site', 'detailed_tumour_site', 'type_of_surgery', 'lymphovascular_invasion',
                        'radiotherapy', 'received_randomised_treatment', 'treatment', 'chemotherapy_full_dose_given',
                        'chemotherapy_dose_reduction_reason', 'chemotherapy_number_cycles_delayed',
                        'chemotherapy_within_6weeks_of_surgery', 'chemotherapy_at_2weekly_intervals', 'DFS_months',
                        'DFS_event', 'DSS_months', 'DSS_event', 'age_bin', 'chemotherapy_cycles_bin', 'cancer_type',
                        'stage_treatment', 'cic_events_class_tumour', 'cic_events_class_invasive_front',
                        'cic_events_class_normal', 'protein_median_CASP8_normal', 'protein_median_CASP8_tumour',
                        'protein_median_DR4_normal', 'protein_median_DR4_tumour', 'protein_median_DR5_normal',
                        'protein_median_DR5_tumour', 'protein_median_FLIP_normal', 'protein_median_FLIP_tumour',
                        'protein_median_TRAIL_normal' and 'protein_median_TRAIL_tumour'.
    """
    # Clinical data
    cli = cli.copy()

    # Cell-in-cell data
    cic_aggregated_tall = (
        cic_aggregated.copy()["median"].stack().to_frame("cic_events_median")
    )
    cic_aggregated_tall = cic_aggregated_tall.assign(
        cic_events_class=(cic_aggregated_tall.cic_events_median == 0).map(
            {True: "CIC=0", False: "CIC>0"}
        )
    )
    cic_aggregated_wide = cic_aggregated_tall.cic_events_class.unstack("tissue")[
        TISSUE_ORDER
    ]
    cic_aggregated_wide.rename(
        columns={
            "normal": "cic_events_class_normal",
            "invasive front": "cic_events_class_invasive_front",
            "tumour": "cic_events_class_tumour",
        },
        inplace=True,
    )

    # Proteins data
    proteins_aggregated = proteins_aggregated.copy()["protein_median"].unstack(
        ["protein", "tissue"]
    )
    proteins_aggregated.columns = [
        "protein_median_" + "_".join(x) for x in proteins_aggregated.columns
    ]

    data = cli.join(cic_aggregated_wide, how="inner").join(proteins_aggregated)

    pathlib.Path("processed_data").mkdir(exist_ok=True)
    data.to_csv(pathlib.Path("processed_data", "clinical_aggregated_cic_proteins.csv"))

    return data
