import pathlib
import typing

import pandas
import statsmodels.formula.api
import statsmodels.stats.anova
import statsmodels.stats.multicomp
import tableone

import py_utils.cox
import py_utils.format_pvalue
import py_utils.kaplan_meier
import py_utils.survival_stats

TISSUE_ORDER = ["tumour", "invasive front", "normal"]


def supplementary_table_1(data: pandas.DataFrame) -> tableone.TableOne:
    """
    Generate supplementary table 1.
    Table include clinical, demographic and pathological characteristics of the patients of the NI240 phase III clinical
    trial.
    :param data: dataframe with clinical data, as returned by `data_util.clinical()`;
    :return: TableOne instance with statistical results comparing cohort features by treatment group.
    """
    data = data.copy().reset_index()
    cols = [
        "radiotherapy",
        "cancer_type",
        "type_of_surgery",
        "stage",
        "T_stage",
        "N_stage",
        "grade",
        "age",
        "sex",
        "lymphovascular_invasion",
    ]
    cat_cols = [
        "radiotherapy",
        "cancer_type",
        "type_of_surgery",
        "stage",
        "T_stage",
        "N_stage",
        "grade",
        "sex",
        "lymphovascular_invasion",
    ]
    num_cols = ["age"]

    # Include only treatment variable to group by and columns of interest
    data = data[["treatment"] + cols]
    pretty_cols = {col: col.replace("_", " ") for col in cols}

    table1 = tableone.TableOne(
        data,
        columns=cols,
        categorical=cat_cols,
        nonnormal=num_cols,
        groupby="treatment",
        pval=True,
        decimals=0,
        labels=pretty_cols,
    )

    table1.tableone.columns = table1.tableone.columns.get_level_values(1)
    table1.tableone.rename(
        columns={"isnull": "Missing [n]", "pval": "P-value", "ptest": "Test"},
        inplace=True,
    )
    table1.tableone.reset_index(inplace=True)
    table1.tableone.columns = table1.tableone.columns.str.capitalize().str.replace(
        "_", "-"
    )

    is_dupplicated = table1.tableone.duplicated(subset="Variable")
    table1.tableone["Variable"] = table1.tableone["Variable"].where(~is_dupplicated, "")

    return table1


def supplementary_table_2(data: pandas.DataFrame) -> pandas.DataFrame:
    """
    Generate supplementary table 2.
    Table include clinical follow-up for the patients of the NI240 phase III clinical trial.
    :param data: dataframe with clinical data, as returned by `data_util.clinical()`;
    :return: TableOne instance with statistical results comparing cohort features by treatment group.
    """
    data = data.copy()
    data["DFS_censored"] = data.DFS_event.map({True: False, False: True})
    data["DSS_censored"] = data.DSS_event.map({True: False, False: True})

    return pandas.concat(
        [
            py_utils.survival_stats.survival_stats(
                data, "DFS_months", "DFS_censored", "DFS"
            ),
            py_utils.survival_stats.survival_stats(
                data, "DSS_months", "DSS_censored", "DSS"
            ),
        ]
    )


def supplementary_table_3(data: pandas.DataFrame) -> pandas.DataFrame:
    """
    Generate supplementary table 3.
    Table include breakdown of the number of tissue microarray cores analyzed for CIC events per patient per tissue for
    patients of the NI240 phase III clinical trial.
    :param data: dataframe with cell-in-cell events data, as returned by `data_util.cic()`;
    :return: dataframe, one row per tissue type ('tumour', 'invasive front', 'normal') and columns 'Tissue',
             'Total # patients', 'Total # cores', 'Median # cores per-patient' and 'Min # cores per-patient'.
    """
    by_patient_tissue1 = (
        data.groupby(["universal_patient_id", "tissue"])
        .size()
        .to_frame("n_cores")
        .n_cores.groupby(["tissue"])
        .describe()[["count", "50%", "min"]]
        .rename(
            columns={
                "count": "total # patients",
                "50%": "median # cores per-patient",
                "min": "min # cores per-patient",
            }
        )
    )

    by_patient_tissue2 = data.groupby(["tissue"]).size().to_frame("total # cores")

    by_patient_tissue = by_patient_tissue2.join(by_patient_tissue1)[
        [
            "total # patients",
            "total # cores",
            "median # cores per-patient",
            "min # cores per-patient",
        ]
    ]
    by_patient_tissue.reset_index(inplace=True)
    by_patient_tissue["tissue"] = pandas.Categorical(
        by_patient_tissue.tissue, TISSUE_ORDER
    )
    by_patient_tissue.sort_values(by=["tissue"], inplace=True)

    # To print nicely
    is_duplicated = by_patient_tissue.duplicated(subset="tissue")
    by_patient_tissue["tissue"] = (
        by_patient_tissue["tissue"].astype("str").where(~is_duplicated, "")
    )

    by_patient_tissue.columns = by_patient_tissue.columns.str.capitalize()

    return by_patient_tissue


def supplementary_table_4(data: pandas.DataFrame) -> pandas.DataFrame:
    """
    Generate supplementary table 4.
    Table include breakdown of the number of tissue microarray cores analyzed for CIC events per marker, tissue and
    slide for patients of the NI240 phase III clinical trial.
    :param data: dataframe with cell-in-cell events data, as returned by `data_util.cic()`;
    :return: dataframe, one row per marker type ('HE', 'cMET'), tissue type ('tumour', 'invasive front', 'normal') and
    slide ('A'-'G') and columns 'Marker', 'Tissue', 'Slide' and '# patients'.
    """
    data_by_marker_tissue_slide = data.copy().reset_index()

    data_by_marker_tissue_slide["tissue"] = pandas.Categorical(
        data_by_marker_tissue_slide.tissue, TISSUE_ORDER
    )
    data_by_marker_tissue_slide["marker"] = pandas.Categorical(
        data_by_marker_tissue_slide.marker, ["HE", "cMET"]
    )
    data_by_marker_tissue_slide["slide"] = pandas.Categorical(
        data_by_marker_tissue_slide.slide, ["A", "B", "C", "D", "E", "F", "G"]
    )

    data_by_marker_tissue_slide = (
        data_by_marker_tissue_slide.groupby(["marker", "tissue", "slide"])
        .size()
        .to_frame("# patients")
        .reset_index()
    )

    # To print nicely
    is_duplicated = data_by_marker_tissue_slide.duplicated(subset=["marker", "tissue"])
    data_by_marker_tissue_slide["marker"] = (
        data_by_marker_tissue_slide["marker"].astype("str").where(~is_duplicated, "")
    )
    data_by_marker_tissue_slide["tissue"] = (
        data_by_marker_tissue_slide["tissue"].astype("str").where(~is_duplicated, "")
    )

    data_by_marker_tissue_slide.columns = (
        data_by_marker_tissue_slide.columns.str.capitalize()
    )

    return data_by_marker_tissue_slide


def supplementary_table_5(data: pandas.DataFrame) -> pandas.DataFrame:
    """
    Generate supplementary table 5.
    Table include breakdown of the number of tissue microarray cores analyzed for CIC events per patient per tissue for
    protein expression by immunohistochemistry for the NI240 phase III clinical trial.
    :param data: dataframe with proteins staining data, as returned by `data_util.proteins()`;
    :return: dataframe, one row per tissue type ('tumour', 'normal') and protein marker ('CASP8', 'DR4', 'DR5', 'FLIP',
             'TRAIL') and columns 'Tissue', 'Protein', 'Total # patients', 'Total # cores',
             'Median # cores per-patient' and 'Min # cores per-patient'.
    """
    data_table = data.copy().reset_index("tissue")
    data_table["tissue"] = pandas.Categorical(data_table.tissue, ["tumour", "normal"])
    data_table.set_index(["tissue"], append=True, inplace=True)

    by_patient_tissue1 = (
        data.groupby(["universal_patient_id", "tissue", "protein"])
        .size()
        .to_frame("n_cores")
        .n_cores.groupby(["tissue", "protein"])
        .describe()[["count", "50%", "min"]]
        .rename(
            columns={
                "count": "total # patients",
                "50%": "median # cores per-patient",
                "min": "min # cores per-patient",
            }
        )
    )

    by_patient_tissue2 = data.groupby("tissue").size().to_frame("total # cores")

    by_patient_tissue = by_patient_tissue2.join(by_patient_tissue1)[
        [
            "total # patients",
            "total # cores",
            "median # cores per-patient",
            "min # cores per-patient",
        ]
    ].reset_index()
    by_patient_tissue["tissue"] = pandas.Categorical(
        by_patient_tissue.tissue, ["tumour", "normal"]
    )
    by_patient_tissue.sort_values(by=["tissue"], inplace=True)

    # To print nicely
    is_duplicated = by_patient_tissue.duplicated(subset="tissue")
    by_patient_tissue["tissue"] = (
        by_patient_tissue["tissue"].astype("str").where(~is_duplicated, "")
    )

    by_patient_tissue.columns = by_patient_tissue.columns.str.capitalize()

    return by_patient_tissue


def _supplementary_table_8_helper(
    cic_aggregated: pandas.DataFrame, proteins_aggregated: pandas.DataFrame
) -> pandas.DataFrame:
    """
    Helper function to prepare data for `supplementary_table8`.
    :param cic_aggregated: dataframe with cell-in-cell events aggregated by patient and tissue, as returned by
           `data_util.aggregate_cic()`;
    :param proteins_aggregated: dataframe with protein staining scores aggregated by patient, tissue and protein
           marker, as returned by `data_util.aggregate_proteins()`;
    :return: dataframe,
    """
    data = (
        cic_aggregated["median"]
        .stack()
        .to_frame("cic_median")
        .join(proteins_aggregated.protein_median, how="inner")
    )
    data["cic_events_class"] = pandas.Categorical(
        (data.cic_median == 0).map({True: "CIC=0", False: "CIC>0"}), ["CIC=0", "CIC>0"]
    )

    return data


def supplementary_table_8(data: pandas.DataFrame) -> pandas.DataFrame:
    """
    Generate supplementary table 8.
    Table include association between expression of proteins involved in TRAIL signalling and CIC events observed in
    tumour tissue of CRC patients of the NI240 phase III clinical trial. Protein expression determined by IHC across
    biological replicas of tumour sections (see `supplementary_table5()`) and per patient were aggregated by the median.
    CIC events observed across multiple biological TMA cores prepared from tumour tissue were aggregated by median and
    binarised into absence (CIC=0) or presence (CIC>0).
    :param data: dataframe,
    :return:
    """

    def helper(df):
        formula = "protein_median ~ cic_events_class"
        model = statsmodels.formula.api.ols(formula, df.reset_index()).fit()
        coef = model.params["cic_events_class[T.CIC>0]"]
        coef_ci = model.conf_int().T["cic_events_class[T.CIC>0]"]
        pvalue = statsmodels.stats.anova.anova_lm(model, typ=1).loc[
            "cic_events_class", "PR(>F)"
        ]

        return "CIC>0 (ref. CIC=0): {:.2f}, 95% CI {:.2f}-{:.2f}, {}, n={:.0f}".format(
            coef,
            coef_ci[0],
            coef_ci[1],
            py_utils.format_pvalue.format_pvalue(pvalue, pvalue_prefix="P"),
            df.shape[0],
        )

    stats = (
        data[["protein_median", "cic_events_class"]]
        .groupby(["tissue", "protein"])
        .apply(helper)
        .to_frame("Unadjusted statistics")
    )

    return stats


def _supplementary_tables_9_and_10_helper(
    data: pandas.DataFrame, cols_to_include_in_multivariate: typing.List[str], name: str
) -> None:
    """
    Helper function for `supplementary_tables9_and_10()`.
    :param data: dataframe with data to include in Cox regression models;
    :param cols_to_include_in_multivariate: list of columns included in 'data' to use for adjustment in multivariate
           models;
    :param name: label to name filename to store output.
    """
    data = data.copy()

    # Workaround for ValueError: Categorical categories must be unique (term categories)
    data.loc[:, "CIC in tumour tissue"] = pandas.Categorical(
        data["CIC in tumour tissue"].replace({"CIC=0": "T-CIC=0", "CIC>0": "T-CIC>0"}),
        ["T-CIC=0", "T-CIC>0"],
    )
    data.loc[:, "CIC in invasive front tissue"] = pandas.Categorical(
        data["CIC in invasive front tissue"].replace(
            {"CIC=0": "S-CIC=0", "CIC>0": "S-CIC>0"}
        ),
        ["S-CIC=0", "S-CIC>0"],
    )
    data.loc[:, "CIC in normal tissue"] = pandas.Categorical(
        data["CIC in normal tissue"].replace({"CIC=0": "N-CIC=0", "CIC>0": "N-CIC>0"}),
        ["N-CIC=0", "N-CIC>0"],
    )

    # Setup Cox regression models for endpoints and uni/multivariate models of interest.
    cox_summary = py_utils.cox.CoxSummaryTable(data)
    cox_summary.add_endpoint("DFS_months", "DFS_event", "DFS")
    cox_summary.add_endpoint("DSS_months", "DSS_event", "DSS")
    cox_summary.add_multivariate_model(cols_to_include_in_multivariate, "multivariate")
    cox_summary.fit(drop_na=True)

    # Export results
    cox_summary.to_html(pathlib.Path("figures_and_tables", f"{name}.html"))


def supplementary_tables_9_and_10(data: pandas.DataFrame) -> None:
    """
    Generate supplementary tables 9 and 10.
    Tables include univariate and multivariate Cox regression models for patients of the NI240 phase III clinical trial,
    either for both stage II and III cases (supplementary table 9) or restricted to stage III only cases (supplementary
    table 10). Univariate models were fitted for baseline clinical, demographic and pathological characteristics and
    features derived from CIC events observed in TMA sections prepared from tumour, invasive front and normal tissue.
    For CIC events-based features, patients were grouped based on the absence (CIC=0) or presence (CIC>0) of CIC events
    computed as median across multiple cores for the corresponding tissue. The multivariate model included CIC events
    feature derived from the invasive front and was adjusted by baseline patient characteristics selected *a priori*.
    :param data: dataframe with data to fit Cox regression models to. Must include columns: 'DFS_months', 'DFS_event',
                 'DSS_months', 'DSS_event', 'stage', 'treatment', 'age', 'sex', 'cancer_type', 'type_of_surgery',
                'cic_events_class_tumour', 'cic_events_class_invasive_front' and 'cic_events_class_normal'.
    """
    cox_data = data[
        [
            "DFS_months",
            "DFS_event",
            "DSS_months",
            "DSS_event",
            "stage",
            "treatment",
            "age",
            "sex",
            "cancer_type",
            "type_of_surgery",
            "cic_events_class_tumour",
            "cic_events_class_invasive_front",
            "cic_events_class_normal",
        ]
    ]
    cox_data = cox_data.rename(
        columns={
            "cic_events_class_tumour": "CIC in tumour tissue",
            "cic_events_class_invasive_front": "CIC in invasive front tissue",
            "cic_events_class_normal": "CIC in normal tissue",
            "cancer_type": "cancer type",
            "type_of_surgery": "type of surgery",
        }
    )

    adjustment_vars = ["stage", "treatment", "age", "sex", "cancer type"]

    _supplementary_tables_9_and_10_helper(
        cox_data,
        ["CIC in invasive front tissue"] + adjustment_vars,
        "supplementary_table_9",
    )
    _supplementary_tables_9_and_10_helper(
        cox_data.query('stage=="3"').drop(columns=["stage"]),
        ["CIC in invasive front tissue"]
        + list(set(adjustment_vars).difference(["stage"])),
        "supplementary_table_10",
    )
