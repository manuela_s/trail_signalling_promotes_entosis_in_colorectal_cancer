[![pythonv3.8](https://img.shields.io/badge/python-v3.8-blue)](https://www.python.org/)
[![License](https://img.shields.io/badge/License-BSD%202--Clause-blue.svg)](https://opensource.org/licenses/bsd-license.php)
[![ZENODO](https://zenodo.org/badge/DOI/10.5281/zenodo.3841833.svg)](http://doi.org/10.5281/zenodo.3841833)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fmanuela_s%2Ftrail_signalling_promotes_entosis_in_colorectal_cancer.git/master)


# TRAIL signalling promotes entosis in colorectal cancer 

Datasets and source-code underlying the computational analyses investigating the clinical association among TRAIL
signalling, cell-in-cell structures, and colorectal cancer included in the manuscript *"TRAIL signalling promotes entosis
in colorectal cancer"* authored by Emir Bozkurt, Heiko Düssmann, Manuela Salvucci, Brenton L. Cavanagh, Sandra Van
Schaeybroeck, Daniel B. Longley, Seamus J. Martin and Jochen H. M. Prehn  (submitted 2020, [biorxiv preprint](https://www.biorxiv.org/content/10.1101/2020.09.29.315317v1)).


## Requirements

Analysis was performed with python 3.8, R 3.6.3 and matlab 2014b (for supplementary figure 6) in Ubuntu 20.04.
The analysis code depends on ubuntu packages listed in [apt.txt](binder/apt.txt), python packages listed in
[requirements.txt](binder/requirements.txt) and the Statistics toolbox in matlab.

A [Dockerfile](binder/Dockerfile) is available for building a docker container with all requirements. 
Matlab is not included in the docker container, and generation of [supplementary figure 6](figures_and_tables/supplementary_figure_6.pdf)
is disabled by default.

The container can be built and run with:

```bash
docker build -f binder/Dockerfile --tag cell_in_cell_clinical_analysis:1.0 .
docker run --publish 8888:8888 --tty --interactive cell_in_cell_clinical_analysis:1.0
```

A pre-built docker image is available in the [Zenodo archive](https://doi.org/10.5281/zenodo.3841833).

Alternatively, the container is available in [Binder](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fmanuela_s%2Ftrail_signalling_promotes_entosis_in_colorectal_cancer.git/master).


## Analysis steps

To reproduce the [results of the analysis](https://www.biorxiv.org/content/biorxiv/early/2020/09/29/2020.09.29.315317/DC3/embed/media-3.pdf?download=true),
please run from the command line `make_summary_pdf.sh`.


## Citation

Please cite https://doi.org/10.5281/zenodo.3841833 and the corresponding paper, if using these datasets and/or source code.


## Contact information

- Prof. Jochen H. M. Prehn (JPrehn@rcsi.ie);
- Manuela Salvucci (manuelasalvucci@rcsi.ie).


## Funding

This work was supported by generous funding from Science Foundation Ireland (14/IA/2582; 16/RI/3740; 16/US/3301) and the
Health Research Board (16/US/330; TRA/2007/26). We wish to sincerely thank the NI240 study participants and trial
investigators.
