#!/usr/bin/env bash

set -e

python run_analysis.py

# Disabled as it requires MATLAB
# matlab -nosplash -nodesktop -r "run('supplementary_figure_6.m'); exit;"

wkhtmltopdf figures_and_tables/supplementary_table_9.html figures_and_tables/supplementary_table_9.pdf
wkhtmltopdf figures_and_tables/supplementary_table_10.html figures_and_tables/supplementary_table_10.pdf

inkscape --export-pdf=figures_and_tables/figure_6_panel_d.pdf figures_and_tables/figure_6_panel_d.svg

for figure in figures_and_tables/*.pdf; do
  pdfcrop "${figure}" "${figure}"
done

pweave -i markdown -f pandoc --figure-directory figures_and_tables/summary summary.pmd

pandoc -i summary.md --filter pandoc-xnos --filter pandoc-citeproc -V papersize=A4 \
       -V geometry:"top=1cm, bottom=2cm, left=1cm, right=1cm" -V fontsize=8pt -V toc=1 -V colorlinks=true \
       -o summary.pdf
