"""Module to compute statistics reported in supplementary tables 6 and 7 included in Supplementary File 1."""

import typing

import numpy
import pandas
import rpy2.robjects
import rpy2.robjects.packages
import rpy2.robjects.pandas2ri

import py_utils.format_pvalue
import py_utils.rpy_helper

base = rpy2.robjects.packages.importr("base")
stats = rpy2.robjects.packages.importr("stats")
car = rpy2.robjects.packages.importr("car")
glmmTMB = rpy2.robjects.packages.importr("glmmTMB")


def _prepare_data_for_stats(
    cli: pandas.DataFrame, cic: pandas.DataFrame
) -> pandas.DataFrame:
    """
    Helper function to prepare data for running stats on.
    :param cli: dataframe with clinical data, as returned by `data_util.clinical()`;
    :param cic: dataframe with cell-in-cell events, as returned by `data_util.cic()`;
    :return: dataframe, one row per patient per tissue type, with clinical and cell-in-cell events data.
    """
    data = cli.join(
        cic.groupby(["cohort", "universal_patient_id", "tissue"]).median().dropna(),
        how="inner",
    ).reset_index()
    data.tissue = pandas.Categorical(
        data.tissue, ["tumour", "invasive front", "normal"]
    )
    data["cohort"] = data.cohort.astype("category")

    data.rename(
        columns={"age_bin": "age", "chemotherapy_cycles_bin": "chemotherapy_cycles"},
        inplace=True,
    )

    return data


def _parse_confint(
    confint: pandas.DataFrame, data: pandas.DataFrame, terms: typing.List[str]
) -> pandas.DataFrame:
    """
    Parse the confint data-frame.
    Conf-int is indexed by a concatenation of the term names and level names.
    :param confint: dataframe, as returned by `stats.confint()` in R;
    :param data: dataframe with clinical and cell-in-cell, as returned by `_prepare_data_for_stats()`;
    :param terms: list of terms included in glmmTMB model;
    :return: dataframe with glmmTMB results per term and adjusted by 'tissue'.
    """

    def get_levels(name: str, s: pandas.Series) -> typing.Iterable[str]:
        """
        Generator for levels for a term in the model.
        :param name: term name;
        :param s: series with the term data;
        :yields: all the levels for the term.
        """
        if isinstance(s.dtype, pandas.api.types.CategoricalDtype):
            for cat in s.cat.categories:
                yield "cond.{}{}".format(name, cat), name, s.cat.categories[0], cat
        else:
            yield "cond.{}".format(name), name, "", ""

    t = pandas.concat(
        [
            pandas.DataFrame(
                get_levels(name, s), columns=["index", "Term", "Ref-level", "Level"]
            )
            for (name, s) in data[terms].items()
        ]
    ).set_index("index")
    t = confint.join(t, how="inner")
    return t


def _run_glmmTMB(data: pandas.DataFrame, terms: typing.List[str]) -> pandas.DataFrame:
    """
    Helper function to run a zero-inflated Poisson regression model, including a random effect for each patient and
    covariate-fixed effects with R package [*glmmTMB*](https://github.com/glmmTMB/glmmTMB) and [car](https://cran.r-project.org/web/packages/car/index.html))
    and return results in a python dataframe.
    :param data: dataframe with clinical and cell-in-cell, as returned by `_prepare_data_for_stats()`;
    :param terms: list of terms to include in glmmTMB model;
    :return: dataframe with results, one row per term/level and columns 'Estimate', '2.5 %', '97.5 %', 'P' and
             '# cores'.
    """
    formula = rpy2.robjects.Formula(
        "events ~ {} + (1|universal_patient_id)".format(" + ".join(terms))
    )
    zi_formula = rpy2.robjects.Formula("~ 1")

    # Convert data to R dataframe
    with rpy2.robjects.conversion.localconverter(
        rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter
    ):
        data_r = rpy2.robjects.conversion.py2rpy(data)

    model = glmmTMB.glmmTMB(
        formula, ziformula=zi_formula, family=stats.poisson, data=data_r
    )

    confint = py_utils.rpy_helper.r_matrix_to_df(stats.confint(model))
    anova = py_utils.rpy_helper.r_matrix_to_df(car.Anova(model, type="III"))
    anova = anova.rename_axis(index="Term").reset_index()
    n_cores = stats.nobs(model)[0]

    t = pandas.concat(
        [_parse_confint(confint, data, terms), anova.iloc[1:]], sort=False
    )
    t.Term = pandas.Categorical(t.Term, terms)  # For ordering
    t.sort_values(["Term", "Ref-level", "Level"], na_position="first", inplace=True)
    t["Term"] = t.Term.astype(
        str
    )  # Convert back to non-categorical to allow concatenation

    t["# cores"] = n_cores
    t.set_index(["Term", "Ref-level", "Level"], inplace=True)

    return t[["Estimate", "2.5 %", "97.5 %", "Pr(>Chisq)", "# cores"]].rename(
        columns={"Pr(>Chisq)": "P"}
    )


def _print_table(t: pandas.DataFrame) -> None:
    """
    Helper function to print a table in latex format.
    :param t: dataframe with the table to print.
    """
    print(
        """\\begin{{table}}[h!]
        {}
        \\end{{table}}
        """.format(
            t.to_latex(
                na_rep="",
                bold_rows=True,
                float_format="%.2f",
                formatters={
                    "P": lambda x: ""
                    if numpy.isnan(x)
                    else py_utils.format_pvalue.format_pvalue(x)
                },
                column_format="p{3cm}p{3cm}p{3cm}rrrrr",
            )
        )
    )


def supplementary_table_6(cli: pandas.DataFrame, cic: pandas.DataFrame) -> None:
    """
    Generate supplementary tables 6.
    Table include dependency of the number of observed cell-in-cell events detected in tissue microarray sections by
    tissue type. For statistical analysis, a zero-inflated Poisson regression model, including a random effect for each
    patient, was fit (R package [glmmTMB](https://github.com/glmmTMB/glmmTMB) and [car](https://cran.r-project.org/web/packages/car/index.html)).
    Number of cores included, effect sizes (estimates), 95% CIs and p-values computed by likelihood ratio tests were
    reported.
    :param cli: dataframe with clinical data, as returned by `data_util.clinical()`;
    :param cic: dataframe with cell-in-cell events, as returned by `data_util.cic()`;
    """

    data = _prepare_data_for_stats(cli, cic)

    _print_table(_run_glmmTMB(data, ["tissue"]))


def supplementary_table_7(cli: pandas.DataFrame, cic: pandas.DataFrame) -> None:
    """
    Generate supplementary tables 7.
    Table include association between number of observed cell-in-cell events detected in TMA sections and clinical,
    demographic or pathological covariates by tissue type. Variables were selected *a priori*. For statistical analysis,
    a zero-inflated Poisson regression model, including a random effect for each patient and covariate-fixed effects,
    was fitted (R package [*glmmTMB*](https://github.com/glmmTMB/glmmTMB) and [car](https://cran.r-project.org/web/packages/car/index.html)).
    Number of cores included, effect sizes (estimates) for the variable of interest, 95% CIs and p-values computed by
    likelihood ratio tests were reported.
    :param cli: dataframe with clinical data, as returned by `data_util.clinical()`;
    :param cic: dataframe with cell-in-cell events, as returned by `data_util.cic()`;
    """
    data = _prepare_data_for_stats(cli, cic)

    stats_results = []
    for term in [
        "stage",
        "T_stage",
        "N_stage",
        "age",
        "sex",
        "type_of_surgery",
        "cancer_type",
        "tumour_site",
        "lymphovascular_invasion",
        "treatment",
        "radiotherapy",
        "chemotherapy_cycles",
    ]:
        stats_results.append(_run_glmmTMB(data, ["tissue", term]))
    stats_results = pandas.concat(stats_results).query('Term!="tissue"')
    stats_results.reset_index(inplace=True)
    stats_results["Term"] = stats_results.Term.str.replace("_", " ")
    stats_results.set_index(["Term", "Ref-level", "Level"], inplace=True)
    _print_table(stats_results)
